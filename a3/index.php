<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
		<meta name="author" content="Jacob Roberts">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Assignment3</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Assignment #3 Requirements:</strong> 
					<p class="text-left">1. Create MySQL ERD</p>
					<p class="text-left">2. Create My Event user interface</p>
					<p class="text-left">3. Provide screenshots</p>
				</p>

				<h4>Screenshot of ERD</h4>
				<img src="img/a3erd.png" class="img-responsive center-block" alt="A3 ERD">

				<h4>First page of app</h4>
				<img src="img/a3firstinterface.png" class="img-responsive center-block" alt="First interface">
                
                <h4>Second page of app</h4>
                <img src="img/a3secondinterface.png" class="img-responsive center-block" alt="Second interface">
                
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
