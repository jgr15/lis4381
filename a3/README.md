# LIS 4381

## Jacob Roberts

### Assignment #3 Requirements:

*Two Parts:*

1. Create MySQL ERD.
2. Create My Event user interface.

#### README.MD file should include the following items:

* Screenshot of database.
* Screenshot of running first interface.
* Screenshot of running second interface.
* Link to a3.mwb.
* Link to a3.sql.


#### Assignment Links:
[a3.mwb](a3.mwb "My a3.mwb file")

[a3.sql](a3.sql "My a3.sql file")


#### Assignment Screenshots:

*Screenshot of Database:*
![Database Screenshot](img/a3ERD.png)

*Screenshot of running first interface:*

![First Interface Screenshot](img/a3firstinterface.png)

*Screenshot of running second interface*:

![Second Interface Screenshot](img/a3secondinterface.png)




#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/jgr15/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/jgr15/myteamquotes/ "My Team Quotes Tutorial")
