# LIS 4381

## Jacob Roberts

### Assignment #5 Requirements:


1. Create Server-side validation.
2. Add datatable to website.

#### README.MD file should include the following items:

* Screenshot of local host page
* Screenshot of serverside validation failed.
* Link to local host website.


#### Assignment Links:
* Link to My Online Portfolio:*
[Local Host Website](http://localhost/repos/lis4381/ "Local Host Website")



#### Assignment Screenshots:


*Screenshot of working table*:

![Local host Screenshot](img/a5table.png)

*Screenshot of error page*:

![Error page Screenshot](img/errorpage.png)





#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/jgr15/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/jgr15/myteamquotes/ "My Team Quotes Tutorial")
