<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
	<meta name="author" content="Jacob Roberts">
	<link rel="icon" href="favicon.ico">

	<title>PHP Simple Calculator</title>

		<?php include_once("css/include_css.php"); ?>	

	<style>
		input[type="text"] { width:80%; }
		section { font-size:1.3em; }
		
	</style>

</head>
<body>

	<?php include_once("global/nav.php"); ?>
	
	<div class="container">
		 <div class="starter-template">
						<div class="page-header">
							<?php include_once("global/header.php"); ?>	

								

						</div>
      
    <section>
    	<h2>Perform Calculation</h2>
    	
    	<form action="calc.php" method="post">
			
			<b>Num1:</b> <input type="text" name="num1"><br><br>
			<b>Num2:</b> <input type="text" name="num2"><br><br>
			
			<input type="radio" name="math" value="add"> Addition
			<input type="radio" name="math" value="subtract"> Subtraction
			<input type="radio" name="math" value="multiply"> Multiplication
			<input type="radio" name="math" value="divide"> Division
			<input type="radio" name="math" value="exponent"> Exponentiation
			
			<br><br>
			
			<input type="submit">
		</form>
	</section>
		
	<hr>
      
      
      
						
<?php
include_once "global/footer.php";
?>

	</div> <!-- end starter-template -->
</div> <!-- end container -->

		<?php include_once("js/include_js.php"); ?>	
	
</body>
</html>
