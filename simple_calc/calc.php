<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
	<meta name="author" content="Jacob Roberts">
	<link rel="icon" href="favicon.ico">

	<title>PHP Simple Calculator</title>

		<?php include_once("css/include_css.php"); ?>	

	<style>
		input[type="text"] { width:80%; }
		section { font-size:1.3em; }
		
	</style>

</head>
<body>

	<?php include_once("global/nav.php"); ?>
	
	<div class="container">
		 <div class="starter-template">
						<div class="page-header">
							<?php include_once("global/header.php"); ?>	

								

						</div>

			<h3> <?php echo $_POST["math"]; ?></h3><br>
			<h3> <!-- continued below: -->

			<?php
			$num1 = $_POST['num1'];
			$num2 = $_POST['num2'];
			$operation = $_POST['math'];

			switch($operation) {
			
			case "add":
			echo "{$num1} + {$num2} = " . ($num1 + $num2);
			break;
			
			case "subtract":
			echo "{$num1} - {$num2} = " . ($num1 - $num2);
			break;

			case "multiply":
			echo "{$num1} * {$num2} = " . ($num1 * $num2);
			break;
			
			case "divide":
			if ( ($num1 == 0) or ($num2 == 0) ) {
				echo "Cannot divide by zero!";
				} else {	
				echo "{$num1} / {$num2} = " . ($num1 / $num2);
				}
			break;
			
			case "exponent":
			echo "{$num1} ^ {$num2} = " . (pow($num1,$num2));
			break;
			
			default:
			break;
			}
			?>
			</h3> <br>			


	
	<br>	<br>
	
<?php
include_once "global/footer.php";
?>

	</div> <!-- end starter-template -->
</div> <!-- end container -->

		<?php include_once("js/include_js.php"); ?>	
	
</body>
</html>
