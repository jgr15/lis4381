# LIS 4381

## Jacob Roberts

### Assignment #2 Requirements:

*Two Parts:*

1. Create user interface
2. Create button link to recipe page

#### README.md file should include the following items:

* Screenshot of running first page of app.
* Screenshot of running second page of app.




#### Assignment Screenshots:

*Screenshot of running first page of app:*

![First Page Screenshot](img/appfirstpage.png)

*Screenshot of running Second page*:

![Second Page Screenshot](img/appsecondpage.png)




#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/jgr15/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/jgr15/myteamquotes/ "My Team Quotes Tutorial")
