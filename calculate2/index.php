<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
	<meta name="author" content="Mark K. Jowett, Ph.D.">
	<link rel="icon" href="favicon.ico">

	<title>Simple Calculator</title>

		<?php include_once("css/include_css.php"); ?>	



</head>
<body>

	<?php include_once("global/nav_global.php") ?>
		<div class="starter-template">
					
					<div class="page-header">
						<?php include_once("global/header.php"); ?>	
					</div>

					<h2>Preform Calculations</h2>

						<form id="defaultForm" method="post" class="form-horizontal" action="#">
								<div class="form-group">
										<label class="col-sm-4 control-label">Num1:</label>
										<div class="col-sm-4">
												<input type="text" class="form-control" name="num1" />
										</div>
								</div>

								<div class="form-group">
									    <label class="col-sm-4 control-label">Num2:</label>
										<div class="col-sm-4">
												<input type="text" class="form-control" name="num2" />
										</div>
								
                                </div>

                                <label class="radio-inline">
                                <input type="radio" name="group1" id="add" value="add" checked="true">add
                                </label>
                                <label class="radio-inline">
                                <input type="radio" name="group1" id="subtract" value="subtract">subtraction
                                </label>
                                <label class="radio-inline">
                                <input type="radio" name="group1" id="times" value="times">muliplication
                                    </label>
                                <label class="radio-inline">
                                <input type="radio" name="group1" id="divide" value="divide">division
                                </label>
                                <label class="radio-inline">
                                <input type="radio" name="group1" id="exponetiate" value="exponentiate">exponentiation
                                </label>

                                <div class="form-group">
									<div class="col-sm-6 col-sm-offset-3">
									<button type="submit" class="btn btn primary" name="calculate" value="Calculate">Calculate</button>
										</div>
								</div>
  		

                        </form>

			

		
  
 
						
<?php
include_once "global/footer.php";
?>

	</div> <!-- end starter-template -->
</div> <!-- end container -->

		<?php include_once("js/include_js.php"); ?>	
	
</body>
</html>
