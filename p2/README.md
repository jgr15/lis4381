# LIS 4381

## Jacob Roberts

### Project #2 Requirements:


1. Create Server-side validation.
2. Add functionality to Edit/Delete buttons from A5.

#### README.MD file should include the following items:

* Screenshot of Project 2
* Screenshot of serverside validation failed.
* Edit Page
* RSS Feed
* Link to local host website.


#### Assignment Links:
* Link to My Online Portfolio:*
[Local Host Website](http://localhost/repos/lis4381/ "Local Host Website")



#### Assignment Screenshots:


*Screenshot of working table*:

![Local host Screenshot](img/p2table.png)

*Screenshot of Edit Page*:

![Edit Page Screenshot](img/edit.png)

*Screenshot of error page*:

![Error page Screenshot](img/errorpage.png)

*Screenshot of RSS Feed*:

![RSS Feed screenshot](img/rssfeed.png)

*Screenshot of Home Page*:

![Home Page Screenshot](img/homepage.png)





#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/jgr15/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/jgr15/myteamquotes/ "My Team Quotes Tutorial")
