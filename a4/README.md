# LIS 4381

## Jacob Roberts

### Assignment #4 Requirements:


1. Create Client-side validation form.
2. Set up local host website.

#### README.MD file should include the following items:

* Screenshot of local host site running.
* Screenshot of a4 form validation failed.
* Screenshot of a4 form validation passed.
* Link to local host website.


#### Assignment Links:
* Link to My Online Portfolio:*
[Local Host Website](http://localhost/repos/lis4381/ "Local Host Website")



#### Assignment Screenshots:


*Screenshot of Local Host Home Page*:

![Local host Screenshot](img/localhosthome.png)

*Screenshot of Failed Validation*:

![Failed Validation Screenshot](img/failedvalidation.png)

*Screenshot of Passed Validation*:

![Passed Validation Screenshot](img/passedvalidation.png)




#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/jgr15/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/jgr15/myteamquotes/ "My Team Quotes Tutorial")
