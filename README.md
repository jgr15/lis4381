

# Lis 4381 - Mobile Web Applications Development

## Jacob Roberts

### Lis 4381 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install AMPPS
    - Install JDK
    - Install Android Studio and create My first App
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorials
    - Provide git command description

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Create user interface
    - Create button to lead to second page of user interface

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Create SQL database
    - Create MyEvent interfaces
    

4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Create Client-side validation form
    - Add pictures and links to carousel
    - Set up local host website

5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Connect MySQL database
    - Add server-side validation
    - Create properly formated table
 

6. [P1 README.md](p1/README.md "My P1 README.md file")
    - Create My Business Card
    - Change background colors
    - Add launcher icon
    - Add border to image and button
    

7. [P2 README.md](p2/README.md "My README.md file")
    - Add funcionality to Delete and Edit buttons
    - Add RSS Feed
    - Add Edit page
    