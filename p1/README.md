# LIS 4381

## Jacob Roberts

### Project 1 Requirements:

*Two Parts:*

1. Create user interface
2. My Business Card
3. Add border to image and button 
4. Change background color
5. add launcher icon

#### README.md file should include the following items:

* Screenshot of running first page of app.
* Screenshot of running second page of app.




#### Assignment Screenshots:

*Screenshot of running first page of app:*

![First Page Screenshot](img/businesscard.png)

*Screenshot of running Second page*:

![Second Page Screenshot](img/businessdetails.png)




#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/jgr15/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/jgr15/myteamquotes/ "My Team Quotes Tutorial")
