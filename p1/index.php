<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
		<meta name="author" content="Jacob Roberts">
    <link rel="icon" href="favicon.ico" />

		<title>LIS4381 - Project #1</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Project #1 Requirements:</strong> 
					<p class="text-left">1. Create user interface</p>
					<p class="text-left">2. Create My Business card</p>
					<p class="text-left">3. Add borders to image and button</p>
					<p class="text-left">4. Change background color</p>
				</p>

				<h4>First page of app</h4>
				<img src="img/businesscard.png" class="img-responsive center-block" alt="Business Card">
                
        <h4>Second page of app</h4>
        <img src="img/businessdetails.png" class="img-responsive center-block" alt="Details of Business Card">
                
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
