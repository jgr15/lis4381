
>
>
>

# LIS 4381

## Jacob Roberts

### Assignment #1 Requirements:

*Three Parts:*

1. Install ampps
2. Install JDK
3. Install Android Studio

#### README.md file should include the following items:

* Screenshot of ampps running
* Screenshot of running JDK Java Hello
* Screenshot of Android studio MyFirst App
* Git commands

> 
> 
> 
>
> #### Git commands w/short descriptions:

1. git commit- create a new local repository
2. git status- lists the files youve changed and those you still need to change
3. git add- add files
4. git commit- adds a commit
5. git push- Sends changes to master branch
6. git pull- fetch changes on the remote server
7. git grep- search the working directory for

#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![AMPPS Installation Screenshot](img/phpinfo.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/hellojava.png)

*Screenshot of Android Studio - My First App*:

![Android Studio Installation Screenshot](img/hellojacobapp.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/jgr15/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/jgr15/myteamquotes/ "My Team Quotes Tutorial")
